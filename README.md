# slicenet
repository to hold all the code and config files related to SliceNet demo

### Multiple workarounds:


Due to some unknown reason OpenWhisk actions fail to communicate with couchdb spawned by https://github.com/apache/openwhisk-deploy-kube helm chart.
Couldn't recreate the issue in 2 of my local environments, using same versions of helm, kubernetes and openwhisk-deploy-kube. 
I suspect calico, but not so familiar with it or its role in the given k8s cluster.
Luckily, openwhisk-deploy-kube helm chart supports external DB and it even states that it is better option for production deployments.

Can be solved by one of 3 options:

* fix the issue in cluster. i would suggest to start with investigating what Calico doing in the cluster

* install your own external couchdb somewhere and specify its details in mycluster.yaml when deploying openwhisk

* install couchdb inside cluster using instructions bellow

### add repo:
``` helm repo add couchdb https://apache.github.io/couchdb-helm ```

### create secret:
```kubectl -n openwhisk create secret generic mycouchdb-couchdb --from-literal=adminUsername=whisk_admin --from-literal=adminPassword=some_passw0rd --from-literal=cookieAuthSecret=some_passw0rd```

### install couchdb. notice it's name should be same as secret name prefix above:
```helm install -n openwhisk mycouchdb couchdb/couchdb --set clusterSize=1 --set createAdminSecret=false --set couchdbConfig.couchdb.uuid=$(curl https://www.uuidgenerator.net/api/version4 2>/dev/null | tr -d -)```

### wait for the couchdb pod to be running:
```kubectl get pods --namespace openwhisk -l "app=couchdb,release=mycouchdb"```

### setup couchdb:
```kubectl exec --namespace openwhisk -it mycouchdb-couchdb-0 -c couchdb --     curl -s     http://127.0.0.1:5984/_cluster_setup     -X POST     -H "Content-Type: application/json"     -d '{"action": "finish_cluster"}'     -u whisk_admin```

### to validate couchdb up and running find its service IP:
```kubectl get svc -n openwhisk```

### and now use the service ip to list all DBs. The command shouldn't fail or hang forever:
```e.g. curl -X GET http://whisk_admin:some_passw0rd@10.97.189.143:5984/_all_dbs```


Second issue. Due to different CPU architecture the action we built and used previously can't work. Server CPU missing AVX instructions required by Tensorflow version. Had to downgrade tensorflow to pretty old release, 1.5, when the AVX CPU instructions not yet mandatory. As well had to make small change in the code to support older version.

### Currently the action uses docker image kpavel/python_mw_runtime:1.5 in public docker repository. In case in the future there a need to rebuild this action to use different image please update [Dockerfile](./kafka_model_wrapper_action/Dockerfile) accoridngly and build it using
```cd kafka_model_wrapper_action
docker build .
```

### To create new action:
```cd kafka_model_wrapper_action
zip -r ktest.zip __main__.py json_sequences_iterator.py model_data/
```

### create action in the openwhisk. notice:
* it's based on the kpavel/python_mw_runtime:1.5
* KAFKA-BROKER specified in parameters should be changed to one used for output result
* OUTPUT-TOPIC should be a topic on KAFKA-BROKER where results should be posted
```
wsk action update ktest1024 -m 1024 --docker kpavel/python_mw_runtime:1.5 ktest.zip -p KAFKA-BROKER 10.98.10.53:9092 -p OUTPUT-TOPIC output-topic
```

### validate the action is working by manually invoking it:
```wsk action invoke ktest1024 -p data '[{"payload":[{"id":"1579600","timestamp":1526461800,"open":false,"correlation_rule":null,"event_count":1,"close_time":1526461614,"geo_agg_code":"2855","entity_class":"LOCAL","entity_id":null,"severity":0,"problem_class":"Loss of Signal","open_time":1526461506,"origin_subsystem":"AWS","entity_technology":"DSLCLIENTE","ticket_id":"","affected_services":"","site_owner":"","site_services":""},{"id":"1589730","timestamp":1526464200,"open":true,"correlation_rule":null,"event_count":1,"close_time":0,"geo_agg_code":"2855","entity_class":"LOCAL","entity_id":null,"severity":0,"problem_class":"Loss of Signal","open_time":1526464181,"origin_subsystem":"AWS","entity_technology":"DSLCLIENTE","ticket_id":"","affected_services":"","site_owner":"","site_services":""},{"id":"1589730","timestamp":1526464500,"open":false,"correlation_rule":null,"event_count":1,"close_time":1526464208,"geo_agg_code":"2855","entity_class":"LOCAL","entity_id":null,"severity":0,"problem_class":"Loss of Signal","open_time":1526464181,"origin_subsystem":"AWS","entity_technology":"DSLCLIENTE","ticket_id":"","affected_services":"","site_owner":"","site_services":""},{"id":"1594525","timestamp":1526466000,"open":false,"correlation_rule":null,"event_count":1,"close_time":1526465780,"geo_agg_code":"2855","entity_class":"LOCAL","entity_id":null,"severity":0,"problem_class":"Loss of Signal","open_time":1526465740,"origin_subsystem":"AWS","entity_technology":"DSLCLIENTE","ticket_id":"","affected_services":"","site_owner":"","site_services":""},{"id":"1597247","timestamp":1526466900,"open":false,"correlation_rule":null,"event_count":1,"close_time":1526466815,"geo_agg_code":"2855","entity_class":"LOCAL","entity_id":null,"severity":0,"problem_class":"Loss of Signal","open_time":1526466694,"origin_subsystem":"AWS","entity_technology":"DSLCLIENTE","ticket_id":"","affected_services":"","site_owner":"","site_services":""}],"windowID":"bc884115-48eb-4a9d-8109-ef998c7166ee","totalRecords":5,"records":5,"totalWindows":1,"sequenceWindow":1,"windowStart":1526461200000,"windowEnd":1526482800000,"length":1886,"min":1526461800000,"max":1526466900000,"groupByObject":{"geo_agg_code":"2855"}}]' --result
```

### above should return empty result, but not fail:
```{
    "Result": ""
}
```

### create input and ouput topics. e.g. using [testclient](https://github.com/helm/charts/tree/master/incubator/kafka#connecting-to-kafka-from-inside-kubernetes) or another external tool
```kubectl -n kafka exec testclient -- kafka-topics --zookeeper my-kafka-zookeeper:2181 --topic input-topic --create --partitions 1 --replication-factor 1
kubectl -n kafka exec testclient -- kafka-topics --zookeeper my-kafka-zookeeper:2181 --topic output-topic --create --partitions 1 --replication-factor 1
```

### deploy openwhisk compose action that on match will send result to output-topic
```
cd kafka_model_wrapper_action
compose kcompose.js > kcompose.json
deploy kcompose kcompose.json -w -i --kind "nodejs:6"
```

### create a trigger listening for messages on Kafka input topic
```wsk trigger create ktopicTrigger -f /whisk.system/messaging/kafkaFeed -p brokers "[\""10.98.10.53:9092\"]" -p topic input-topic```

### create a rule to bind trigger to compose action
```wsk rule create krule ktopicTrigger kcompose```

### start listen on the output-topic in one console
```kubectl -n slicenet exec -ti testclient -- ./bin/kafka-console-consumer.sh --bootstrap-server 10.98.10.53:9092 --topic output-topic --from-beginning```

### post a message with result to input-topic from another console[attachment "hit" deleted by Kenneth Nagin/Haifa/IBM] 
```kubectl -n slicenet exec -ti testclient -- bash -c "cat /opt/kafka/hit | /opt/kafka/bin/kafka-console-producer.sh --broker-list 10.98.10.53:9092 --topic input-topic"```

# relatively shortly, you should see output like this in the consumer console
```
ibm@slnet-it-jumphost:~$ kubectl -n slicenet exec -ti testclient -- ./bin/kafka-console-consumer.sh --bootstrap-server 10.98.10.53:9092 --topic output-topic --from-beginning
{"prediction": {"TTE": 22034.94529724121, "event": true, "probability": 1}, "context_data": {"totalRecords": 32, "windowStart": 1526461200000, "windowEnd": 1526482800000, "dayhour": [-0.7071067811865475, 0.7071067811865476], "weekday": [-0.09801714032956042, 0.9951847266721969], "geo_agg_code": "5356"}}
```
