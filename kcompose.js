const composer = require('openwhisk-composer')

module.exports = composer.if_nosave(
    composer.action('/guest/ktest1024'),
    composer.action('/whisk.system/messaging/kafkaProduce'),//composer.action('/guest/success'),  //once it works we place kafka call here
    composer.action('/guest/failure', { action: function (param) { console.log("param", param);return { message: 'failure' } } }))
